package edu.cankaya.seng102.homeproject.manager;

import edu.cankaya.seng102.homeproject.personnel.Personnel;

import java.util.Collection;
import java.util.Map;

public interface PaymentManager {

    Map<String, Double> calculatePayments(Collection<Personnel> personnels);

    void printPayment(Map<String, Double> paymentMap, PersonnelManager personnelManager);
}
