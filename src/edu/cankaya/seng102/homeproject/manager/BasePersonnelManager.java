package edu.cankaya.seng102.homeproject.manager;

import edu.cankaya.seng102.homeproject.exception.SSNExistsException;
import edu.cankaya.seng102.homeproject.exception.SSNNotFoundException;
import edu.cankaya.seng102.homeproject.personnel.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class BasePersonnelManager implements PersonnelManager {
    private List<Personnel> personnelList;
    private int premiumPercent;

    BasePersonnelManager(int premiumPercent) {
        this.premiumPercent = premiumPercent;
        personnelList = new ArrayList<>();
    }

    private int getPremiumPercent() {
        return premiumPercent;
    }

    @Override
    public void load(String csvFile) {
        File file = new File(csvFile);
        try (FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            String[] personnelInfo;
            double totalMonthlySales = 0;
            while ((line = bufferedReader.readLine()) != null &&
                    (personnelInfo = line.split(", ")).length == 7) {
                switch (personnelInfo[3]) {
                    case "Manager":
                        personnelList.add(new Manager(personnelInfo[0], personnelInfo[1], personnelInfo[2],
                                Double.parseDouble(personnelInfo[4]), Double.parseDouble(personnelInfo[5]),
                                Double.parseDouble(personnelInfo[6]), getPremiumPercent()));
                        break;
                    case "Intern":
                        personnelList.add(new Intern(personnelInfo[0], personnelInfo[1], personnelInfo[2],
                                Double.parseDouble(personnelInfo[4]), Double.parseDouble(personnelInfo[5]),
                                Double.parseDouble(personnelInfo[6])));
                        break;
                    case "Officer":
                        personnelList.add(new Officer(personnelInfo[0], personnelInfo[1], personnelInfo[2],
                                Double.parseDouble(personnelInfo[4]), Double.parseDouble(personnelInfo[5]),
                                Double.parseDouble(personnelInfo[6])));
                        break;
                    case "Salesman":
                        personnelList.add(new Salesman(personnelInfo[0], personnelInfo[1], personnelInfo[2],
                                Double.parseDouble(personnelInfo[4]), Double.parseDouble(personnelInfo[5]),
                                Double.parseDouble(personnelInfo[6]), getPremiumPercent()));
                        totalMonthlySales += Double.parseDouble(personnelInfo[6]);
                        break;
                }
            }
            for (Personnel personnel: personnelList) {
                if (personnel instanceof Manager) {
                     ((Manager) personnel).setMonthlySalePayment(totalMonthlySales);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void write(String csvFile) {
        File file = new File(csvFile);
        try (
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            PrintWriter printWriter = new PrintWriter(bufferedWriter)) {
            for (Personnel personnel: personnelList) {
                printWriter.write(String.format("%s\n", personnel.toString()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Personnel> listAll() {
        return personnelList;
    }

    @Override
    public Personnel bySSN(String ssn) throws SSNNotFoundException {
        for (Personnel personnel: personnelList) {
            if (personnel.getSSN().equals(ssn)) {
                return personnel;
            }
        }
        throw new SSNNotFoundException("there's not any personnel with the given SSN");
    }

    @Override
    public void create(Personnel personnel) throws SSNExistsException {
        for (Personnel existingPersonnel: personnelList) {
            if (existingPersonnel.getSSN().equals(personnel.getSSN())) {
                throw new SSNExistsException("there's a personnel with the same SSN");
            }
        }
        personnelList.add(personnel);
    }

    @Override
    public void update(Personnel personnel) throws SSNNotFoundException {
        for (int i = 0; i < personnelList.size(); i++) {
            if (personnelList.get(i).getSSN().equals(personnel.getSSN())) {
                personnelList.set(i, personnel);
            }
        }
        throw new SSNNotFoundException("there's not any personnel with the given SSN");
    }

    @Override
    public void delete(String ssn) throws SSNNotFoundException {
        boolean removed = false;
        for (int i = 0; i < personnelList.size(); i++) {
            if (personnelList.get(i).getSSN().equals(ssn)) {
                personnelList.remove(i);
                removed = true;
                break;
            }
        }
        if (!removed) {
            throw new SSNNotFoundException("there's not any personnel with the given SSN");
        }
    }
}
