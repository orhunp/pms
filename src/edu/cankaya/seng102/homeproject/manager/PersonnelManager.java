package edu.cankaya.seng102.homeproject.manager;

import edu.cankaya.seng102.homeproject.exception.SSNExistsException;
import edu.cankaya.seng102.homeproject.exception.SSNNotFoundException;
import edu.cankaya.seng102.homeproject.personnel.Personnel;

import java.util.List;

public interface PersonnelManager {

    void load(String csvFile);

    void write(String csvFile);

    List<Personnel> listAll();

    Personnel bySSN(String ssn) throws SSNNotFoundException;

    void create(Personnel personnel) throws SSNExistsException;

    void update(Personnel personnel) throws SSNNotFoundException;

    void delete(String ssn) throws SSNNotFoundException;
}
