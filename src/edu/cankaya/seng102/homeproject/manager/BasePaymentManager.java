package edu.cankaya.seng102.homeproject.manager;

import edu.cankaya.seng102.homeproject.exception.SSNNotFoundException;
import edu.cankaya.seng102.homeproject.personnel.Personnel;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class BasePaymentManager implements PaymentManager {

    @Override
    public Map<String, Double> calculatePayments(Collection<Personnel> personnels) {
        Map<String, Double> payments = new HashMap<>();
        for (Personnel personnel: personnels) {
            payments.put(personnel.getSSN(), personnel.calculatePayment());
        }
        return payments;
    }

    @Override
    public void printPayment(Map<String, Double> paymentMap, PersonnelManager personnelManager) {
        NumberFormat formatter = new DecimalFormat("#,###.###");
        for(Map.Entry<String, Double> entry : paymentMap.entrySet()) {
            try {
                System.out.println(String.format("%s: %s", personnelManager.bySSN(entry.getKey()),
                        formatter.format(entry.getValue())));
            } catch (SSNNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
