package edu.cankaya.seng102.homeproject.personnel;

public interface Personnel {

    String getSSN();

    double calculatePayment();
}
