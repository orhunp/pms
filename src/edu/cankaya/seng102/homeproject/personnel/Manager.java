package edu.cankaya.seng102.homeproject.personnel;

public class Manager implements Personnel {

    private String SSN, name, surname;
    private Double baseSalary, foodPayment, monthlySales, monthlySalePayment = 0.0;
    private int premiumPercent;

    public Manager(String SSN, String name, String surname, Double baseSalary,
                   Double foodPayment, Double monthlySales, int premiumPercent) {
        this.SSN = SSN;
        this.name = name;
        this.surname = surname;
        this.baseSalary = baseSalary;
        this.foodPayment = foodPayment;
        this.monthlySales = monthlySales;
        this.premiumPercent = premiumPercent;
    }

    @Override
    public String getSSN() {
        return SSN;
    }

    private String getName() {
        return name;
    }

    private String getSurname() {
        return surname;
    }

    private Double getBaseSalary() {
        return baseSalary;
    }

    private Double getFoodPayment() {
        return foodPayment;
    }

    private Double getMonthlySales() {
        return monthlySales;
    }

    public void setMonthlySalePayment(Double monthlySalePayment) {
        this.monthlySalePayment = monthlySalePayment;
    }

    private Double getMonthlySalePayment() {
        return monthlySalePayment;
    }

    private int getPremiumPercent() {
        return premiumPercent;
    }

    @Override
    public double calculatePayment() {
        return baseSalary + (getMonthlySalePayment() * getPremiumPercent()) / 100;
    }

    @Override
    public String toString() {
        return String.format("%s, %s, %s, Manager, %.0f, %.0f, %.0f", getSSN(), getName(),
                getSurname(), getBaseSalary(), getFoodPayment(), getMonthlySales());
    }
}
