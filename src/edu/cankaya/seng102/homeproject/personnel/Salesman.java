package edu.cankaya.seng102.homeproject.personnel;

public class Salesman implements Personnel {

    private String SSN, name, surname;
    private Double baseSalary, foodPayment, monthlySales;
    private int premiumPercent;

    public Salesman(String SSN, String name, String surname, Double baseSalary,
                    Double foodPayment, Double monthlySales, int premiumPercent) {
        this.SSN = SSN;
        this.name = name;
        this.surname = surname;
        this.baseSalary = baseSalary;
        this.foodPayment = foodPayment;
        this.monthlySales = monthlySales;
        this.premiumPercent = premiumPercent;
    }

    @Override
    public String getSSN() {
        return SSN;
    }

    private String getName() {
        return name;
    }

    private String getSurname() {
        return surname;
    }

    private Double getBaseSalary() {
        return baseSalary;
    }

    private Double getFoodPayment() {
        return foodPayment;
    }

    private Double getMonthlySales() {
        return monthlySales;
    }

    private int getPremiumPercent() {
        return premiumPercent;
    }

    @Override
    public double calculatePayment() {
        return (getMonthlySales() * getPremiumPercent()) / 100;
    }

    @Override
    public String toString() {
        return String.format("%s, %s, %s, Salesman, %.0f, %.0f, %.0f", getSSN(), getName(),
                getSurname(), getBaseSalary(), getFoodPayment(), getMonthlySales());
    }
}
