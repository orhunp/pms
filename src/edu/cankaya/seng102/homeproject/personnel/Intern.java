package edu.cankaya.seng102.homeproject.personnel;

public class Intern implements Personnel {

    private String SSN, name, surname;
    private Double baseSalary, foodPayment, monthlySales;

    public Intern(String SSN, String name, String surname, Double baseSalary,
                  Double foodPayment, Double monthlySales) {
        this.SSN = SSN;
        this.name = name;
        this.surname = surname;
        this.baseSalary = baseSalary;
        this.foodPayment = foodPayment;
        this.monthlySales = monthlySales;
    }

    @Override
    public String getSSN() {
        return SSN;
    }

    private String getName() {
        return name;
    }

    private String getSurname() {
        return surname;
    }

    private Double getBaseSalary() {
        return baseSalary;
    }

    private Double getFoodPayment() {
        return foodPayment;
    }

    private Double getMonthlySales() {
        return monthlySales;
    }

    @Override
    public double calculatePayment() {
        return getFoodPayment();
    }

    @Override
    public String toString() {
        return String.format("%s, %s, %s, Intern, %.0f, %.0f, %.0f", getSSN(), getName(),
                getSurname(), getBaseSalary(), getFoodPayment(), getMonthlySales());
    }
}
