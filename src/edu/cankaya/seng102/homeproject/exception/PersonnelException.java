package edu.cankaya.seng102.homeproject.exception;

public class PersonnelException extends Exception {

    private String message;

    public PersonnelException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() { return message; }
}
