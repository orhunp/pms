package edu.cankaya.seng102.homeproject.exception;

public class SSNExistsException extends PersonnelException {

    public SSNExistsException(String message) {
        super(message);
    }
}
