package edu.cankaya.seng102.homeproject.exception;

public class SSNNotFoundException extends PersonnelException {

    public SSNNotFoundException(String message) {
        super(message);
    }
}
