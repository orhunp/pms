package edu.cankaya.seng102.homeproject.manager;

import edu.cankaya.seng102.homeproject.exception.SSNExistsException;
import edu.cankaya.seng102.homeproject.exception.SSNNotFoundException;
import edu.cankaya.seng102.homeproject.personnel.Officer;
import edu.cankaya.seng102.homeproject.personnel.Salesman;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class BasePersonnelManagerTest {

    private BasePersonnelManager basePersonnelManager = new BasePersonnelManager(20);
    private String csvFile = String.format("%s%s%s", System.getProperty("user.dir"), File.separator, "example.csv");

    @Test
    void load() {
        basePersonnelManager.load(csvFile);
        assertEquals(4, basePersonnelManager.listAll().size());
    }

    @Test
    void write() {
        basePersonnelManager.load(csvFile);
        basePersonnelManager.write(csvFile);
        assertEquals(4, basePersonnelManager.listAll().size());
    }

    @Test
    void listAll() {
        basePersonnelManager.load(csvFile);
        assertEquals(4, basePersonnelManager.listAll().size());
    }

    @Test
    void bySSN() {
        basePersonnelManager.load(csvFile);
        assertThrows(SSNNotFoundException.class, () -> basePersonnelManager.bySSN("0"));
    }

    @Test
    void create() {
        basePersonnelManager.load(csvFile);
        try {
            basePersonnelManager.create(new Officer("1", "", "",
                    0.0, 0.0, 0.0));
        } catch (SSNExistsException e) {
            e.printStackTrace();
        }
        assertEquals(5, basePersonnelManager.listAll().size());
        assertThrows(SSNExistsException.class, () -> basePersonnelManager.create(new Officer("12345",
                "", "", 0.0, 0.0, 0.0)));
    }

    @Test
    void update() {
        basePersonnelManager.load(csvFile);
        assertThrows(SSNNotFoundException.class, () -> basePersonnelManager.update(new Salesman("0",
                "", "",0.0, 0.0, 0.0, 20)));
    }

    @Test
    void delete() {
        basePersonnelManager.load(csvFile);
        try {
            basePersonnelManager.delete("12345");
        } catch (SSNNotFoundException e) {
            e.printStackTrace();
        }
        assertEquals(3, basePersonnelManager.listAll().size());
        assertThrows(SSNNotFoundException.class, () -> basePersonnelManager.delete("12345"));
    }
}