package edu.cankaya.seng102.homeproject.manager;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class BasePaymentManagerTest {

    private BasePaymentManager basePaymentManager = new BasePaymentManager();
    private BasePersonnelManager basePersonnelManager = new BasePersonnelManager(20);
    private String csvFile = String.format("%s%s%s", System.getProperty("user.dir"), File.separator, "example.csv");

    @Test
    void calculatePayments() {
        basePersonnelManager.load(csvFile);
        Map<String, Double> payments = basePaymentManager.calculatePayments(basePersonnelManager.listAll());
        for (int i = 0; i < payments.size(); i++) {
            assertEquals(payments.keySet().toArray()[i], basePersonnelManager.listAll().get(i).getSSN());
        }
        assertEquals(payments.get("12340").doubleValue(), 7000);
        assertEquals(payments.get("12345").doubleValue(), 150);
        assertEquals(payments.get("12346").doubleValue(), 1150);
        assertEquals(payments.get("12347").doubleValue(), 2000);
    }

    @Test
    void printPayment() {
        basePersonnelManager.load(csvFile);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(byteArrayOutputStream));
        Map<String, Double> payments = basePaymentManager.calculatePayments(basePersonnelManager.listAll());
        basePaymentManager.printPayment(payments, basePersonnelManager);
        assertEquals("12340, Hayri, Sever, Manager, 5000, 0, 0: 7,000\r\n" +
                "12345, Ali, Seydi, Intern, 0, 150, 0: 150\r\n" +
                "12346, Hüseyin, Temuçin, Officer, 1000, 150, 0: 1,150\r\n" +
                "12347, Aydın, Kaya, Salesman, 0, 0, 10000: 2,000\r\n", byteArrayOutputStream.toString());
    }
}